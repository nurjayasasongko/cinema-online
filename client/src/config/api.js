import axios from 'axios';

export const API = axios.create({
  baseURL: "http://66.42.51.85:5001/api/v1/"
});

export const setAuthToken = (token) => {
  if (token) {
    API.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete API.defaults.headers.common["Authorization"]
  }
};
